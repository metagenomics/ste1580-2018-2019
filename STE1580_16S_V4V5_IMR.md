Descargar Paolinelli16S.zip y PaolinelliITS.zip files from link in dropbox provided by [IMR Sequencing facility](https://imr.bio/) and backup in [Drive](https://drive.google.com/drive/folders/1pTnYk-g9Z0dvn1hj_2i6Pmf6RZvfmfmo?usp=sharing)

Descomprimir y preparar archivos para analisis con mothur

```
ls *R1*fastq | sort > R1_names
ls *R2*fastq | sort > R2_names
cut -d "_" -f1 R1_names > codes
paste codes R1_names R2_names > 16S_STE1580_230919.files
```
Luego es necesario preparar la referencia de silva_v132 para que quede optimizada a la región amplificada V4_V5 mediante el archivo [V4V5prep_ref_4_mothur.c](https://bitbucket.org/metagenomics/data_collection_info/src/master/V4V5_prep_ref_4_mothur.c)
```
/usr/bin/mothur V4V5_prep_ref_4_mothur.c
```
Finalmente correr el análisis completo usando el archivo [V4V5_mothur.c](https://bitbucket.org/metagenomics/data_collection_info/src/master/V4V5_mothur.c)
```
/usr/bin/mothur V4V5_mothur.c
```
Para construir una gráfica tipo Krona compuesta por un gráfico de tarta para cada una de las muestras  usar [mothur_krona_XML.py](https://bitbucket.org/metagenomics/ste1580-2018-2019/src/b2aceacae05912f146a5fd38bae9b9603eea6bb4/mothur_krona_XML.py)
```
/usr/bin/python2.7 mothur_krona_XML.py *list.0.03.cons.tax.summary > 4_krona.xml 
ktImportXML 4_krona.xml
```
Para generar los archivos biom que seran usados para ser visualizados en [Phinch](http://phinch.org/)
```
mothur '#make.biom(shared=16S_STE1580_230919.trim.contigs.good.unique.good.filter.unique.precluster.abund.pick.pick.phylip.an.unique_list.shared, constaxonomy=16S_STE1580_230919.trim.contigs.good.unique.good.filter.unique.precluster.abund.pick.pick.phylip.an.unique_list.0.01.cons.taxonomy, metadata=metadata_4_biom)'
```
Para explorar los aspectos funcionales utilice [FAPROTAX](http://www.loucalab.com/archive/FAPROTAX/lib/php/index.php?section=Instructions)
```
/home/quetjaune/miniconda3/bin/python /home/quetjaune/FAPROTAX_1.2.3/collapse_table.py -i 16S_STE1580.biom_w_tax_ed.txt -o func_table.tsv -g /home/quetjaune/FAPROTAX_1.2.3/FAPROTAX.txt -d "taxonomy" -c "#" -v
```
Luego para graficar los resultados de las funciones bacterianas

