#Download PaolinelliITS.zip file from [Google Drive](https://drive.google.com/drive/folders/1pTnYk-g9Z0dvn1hj_2i6Pmf6RZvfmfmo?usp=sharing) and uncompress

Preparar archivo para indicar relación entre nombre de muestras y archivos fastq
```
paste <(ls *R1*fastq | sort | cut -d "_" -f1 | cut -d "-" -f2) <(ls *R1*fastq | sort) <(ls *R2*fastq | sort) > ITS2_IMR230919.files
```
Luego descargar la referencia para ITS2 de [UNITE](https://unite.ut.ee/repository.php) usar procolo en mothur mediante el archivo [ITS2_mothur.c](https://bitbucket.org/metagenomics/data_collection_info/src/e5ad30e933579231aef49ad3a35c3263c96914f6/ITS2_mothur.c)

Para construir una gráfica tipo Krona compuesta por un gráfico de tarta para cada una de las muestras  usar [mothur_krona_XML.py](https://bitbucket.org/metagenomics/ste1580-2018-2019/src/b2aceacae05912f146a5fd38bae9b9603eea6bb4/mothur_krona_XML.py)
```
/usr/bin/python2.7 mothur_krona_XML.py ITS2_IMR230919.trim.contigs.good.unique.pick.pick.agc.unique_list.0.055.pick.0.055.cons.tax.summary > ITS2_krona.xml 
ktImportXML ITS2_krona.xml -o ITS2_krona.html
```
Para generar los archivos biom que seran usados para ser visualizados en [Phinch](http://phinch.org/)
```
mothur '#make.biom(shared=ITS2*.unique_list.shared, constaxonomy=ITS2*.an.unique_list.0.01.cons.taxonomy, metadata=metadata_4_biom)'
```
Luego para utilizar FUNGUILD es necesario transformar el archivo biom mediante biom convert y luego mediante el script de python [funguilds_v1.0.py](https://bitbucket.org/metagenomics/ste1580-2018-2019/src/da8644171172655aecdc102f67cd9caead4bf44e/funguilds_v1.0.py)

```
biom convert -i ITS2_IMR230919.trim.contigs.good.unique.pick.pick.agc.unique_list.0.055.pick.0.055.pick.0.055.biom -o table.from_biom_w_taxonomy.txt --to-tsv --header-key taxonomy
python funguilds_v1.0.py -otu table.from_biom_w_taxonomy.txt -db fungi -m -u
awk '{count=0; for (col=2; col<=15; col++) ($col >= 5) ? count++ : 0; if (count>=3) print}' table.from_biom_w_taxonomy.guilds_matched.txt > guilds_matched_3colhigh5.txt
```
Luego esto lo procese mediante [funguild_plots.R](https://bitbucket.org/metagenomics/ste1580-2018-2019/src/da8644171172655aecdc102f67cd9caead4bf44e/funguild_plots.R) para obtener graficas que representen los principales roles ecológicos de los hongos identificados en nuestros datos