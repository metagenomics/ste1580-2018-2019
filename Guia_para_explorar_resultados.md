Caracterización del microbioma del suelo
========================================

Objetivos del proyecto (STE1580)

Objetivo 1. Identificar y determinar cantidad relativa de microorganismos del suelo del viñedo (bacterias y hongos)

Objetivo 2. Correlacionar las abundancias relativas de los microorganismos encontrados con las características fisico-químicas del sitio de muestreo y manejo agrícola del mismo.

Ojetivo 3. Obtener lista de microorganismos con capacidad benéfica (Bacterias promotoras de crecimiento patogénica, con capacidad de solubilizar fosforo, etc) y recomendación de tratamiento para aumentar su población.

Muestreo
--------
Se hicieron dos muestreos de suelo en zonas completamente diferentes: Gualtallary y Santa Rosa.

![mapa_muestreo](https://drive.google.com/uc?export=view&id=1I-NRaQcOoRrZQz-QGX6zVIvX_sNkBIBe)

mapa con más detalle en Gualtallary: https://www.google.com/maps/d/u/0/edit?mid=1LxrwNYYlDg6JrkEXMATEw5g8sH9GBN-5&ll=-33.40810918555407%2C-69.22485399042131&z=18

mapa con más detalles en Santa Rosa: https://www.google.com/maps/d/u/0/edit?mid=1VfMPy-ziw4kAA0bSzfj-gsCiNViqR9ue&ll=-33.27733469870622%2C-68.14009669952594&z=14

### Codificación de muestras ###

Código | Tipo de muestra | Cuartel | Sector | Réplica | Número (fecha) | Coordenadas | Varietal/Cultivar | Características
--- | --- | --- | --- | --- | --- | --- | --- | ---
Gp1 | S (suelo) | Inculto aledaño Gualtallary | - | 1 | 7.3.19 | -33.4082, -69.22499 | - | capa de roca > 1 m de profundidad
Gp2 | S (suelo) | Inculto aledaño Gualtallary | - | 2 | 7.3.19 | -33.40832, -69.22509 | - | capa de roca > 1 m de profundidad
Gp3 | S (suelo) | Inculto aledaño Gualtallary | - | 3 | 7.3.19 | -33.40868, -69.22537 | - |capa de roca > 1 m de profundidad
Gs1 | S (suelo) | Inculto aledaño Gualtallary | - | 1 | 7.3.19 | -33.40777, -69.22401 | - | capa de roca < 1 m de profundidad
Gs2 | S (suelo) | Inculto aledaño Gualtallary | - | 2 | 7.3.19 | -33.40826, -69.22366 | - | capa de roca < 1 m de profundidad
Gs3 | S (suelo) | Inculto aledaño Gualtallary | - | 3 | 7.3.19 | -33.40849, -69.22336 | - | capa de roca < 1 m de profundidad
Sra1 | S (suelo) | cultivado Santa Rosa | ? | 1 | 10.8.18 | -33,25781,-68,13377 | Malbec | 1 año Malbec cot/con barbecho alfalfa 3 años previos
Sra2 | S (suelo) | cultivado Santa Rosa | ? | 2 | 10.8.18 | -33,25865,-68,13258 | Malbec | 1 año Malbec cot/con barbecho alfalfa 3 años previos
Sra3 | S (suelo) | cultivado Santa Rosa | ? | 3 | 10.8.18 | -33,25941,-68,13151 | Malbec | 1 año Malbec cot/con barbecho alfalfa 3 años previos
Srb1 | S (suelo) | cultivado Santa Rosa | ? | 1 | 10.8.18 | -33,25845,-6813402 | Malbec | 1 año Malbec cot/sin barbecho de alfalfa
Srb2 | S (suelo) | cultivado Santa Rosa | ? | 2 | 10.8.18 | -33,25906,-68,13290 | Malbec | 1 año Malbec cot/sin barbecho de alfalfa
Srb3 | S (suelo) | cultivado Santa Rosa | ? | 3 | 10.8.18 | -33,25990,-68,13177 | Malbec | 1 año Malbec cot/sin barbecho de alfalfa
Srd1 | S (suelo) | Inculto aledaño Santa Rosa | ? | 1 | 10.8.18 | ? | - | proximo al rio tupungato
Srd2 | S (suelo) | Inculto aledaño Santa Rosa | ? | 2 | 10.8.18 | ? | - | proximo al rio tupungato
Srd3 | S (suelo) | Inculto aledaño Santa Rosa | ? | 3 | 10.8.18 | ? | - | proximo al rio tupungato

### Resultados y gráficas generales obtenidas mediante phyloseq ###

Procariotas
-----------
![bact_divers](https://drive.google.com/uc?export=view&id=1RfDc-VpQC3pemw2op4yNtgQLDow4Otwu)
![NMDS_proc](https://drive.google.com/uc?export=view&id=1uQy456sBtjYYk533QcoTIAn83R5e31tQ)
![signif_proc_diff](https://drive.google.com/uc?export=view&id=1WDiFd602qz9VKhQKD1FYGVisiBzC2-fT)
![proc_benef](https://drive.google.com/uc?export=view&id=1OO-5CZLzgtE5cL_K0ws9Ir7RyyZ-8foV)

Hongos
------
![divers_hongos](https://drive.google.com/uc?export=view&id=16Ogar4zV7uNFGEHNaz1evQnNv9Mm9JIw)
![divers_abund_hongos](https://drive.google.com/uc?export=view&id=1JrjG4XCW3-86OvwQUAuExvGVl6IbLHPO)
![NMDS_hongos](https://drive.google.com/uc?export=view&id=1A6OqDf6fahPl5resYZjpUiBY_b8AcdYR)
![signif_fungi_diff](https://drive.google.com/uc?export=view&id=11rMVHnwUiWsZciOek_7jRrKc7C7mFzQP)
![hongos_patog](https://drive.google.com/uc?export=view&id=1IOLqb2ub9kvVOB3DXXnw7qKDB8Him8s4)



### Microorganismos identificados y abundancia proporcional en el microbioma ###
[Bacterias/Arqueas](https://drive.google.com/uc?id=1_5AcTrdDPtYb4YCExqAv1UnpW8x-nE3I) | [Hongos](https://drive.google.com/uc?id=1nW4_rNbqCTG7vlb17nh8BSV8gu4pk-xM)

### Gráficos individuales ###

[Gp1](https://drive.google.com/uc?id=1d_tsun_eFUgxxEvVulV-jsKdCnC-ZsvL)
[Gp2]()
[Gp3]()
[Gs1]()
[Gs2]()
[Gs3]()
[Sra1]()
[Sra2]()
[Sra3]()
[Srb1]()
[Srb2]()
[Srb3]()
[Srd1]()
[Srd2]()
[Srd3]()

### Para explorar los resultados más en detalles en [Phinch](http://phinch.org/) ###
Descargar la app del programa y luego descargar los archivos biom que se utilizarán en el mismo
[Bacterias/Arqueas](https://drive.google.com/uc?id=1vd___KJFMk3JA4mbB0iprsjF3E8dERUj) | [Hongos](https://drive.google.com/uc?id=10KhGIgFa0OFAmiDQZGYkde5_M7TarQRB)













